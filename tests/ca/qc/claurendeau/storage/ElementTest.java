package ca.qc.claurendeau.storage;

import org.junit.Assert;
import org.junit.Test;

public class ElementTest {


    @Test
    public void getDataTest() {
        int dataExpected = 1;
        Element element = new Element();
        element.setData(dataExpected);

        Assert.assertEquals(dataExpected, element.getData());
    }

}
