package ca.qc.claurendeau.storage;

import static org.junit.Assert.*;

import ca.qc.claurendeau.exception.BufferEmptyException;
import ca.qc.claurendeau.exception.BufferFullException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class BufferTest {


    @Test
    public void capacityTest() {
        int bufferCapacity = 1;
        Buffer buffer = new Buffer(bufferCapacity);
        Assert.assertEquals(bufferCapacity , buffer.capacity());
    }

    @Test
    public void capacityTestZero() {
        int bufferCapacity = 0;
        Buffer buffer = new Buffer(bufferCapacity);
        Assert.assertEquals(bufferCapacity, buffer.capacity());
    }

    @Test
    public void capacityTestMany() {
        int bufferCapacity = 2;
        Buffer buffer = new Buffer(bufferCapacity);
        Assert.assertEquals(bufferCapacity, buffer.capacity());
    }

    @Test
    public void isEmptyTest() {
        int bufferCapacity = 1;
        Buffer buffer = new Buffer(bufferCapacity);
        Assert.assertTrue(buffer.isEmpty());

    }

    @Test
    public void isEmptyOneElementTest() throws BufferFullException{
        int bufferCapacity = 1;
        Buffer buffer = new Buffer(bufferCapacity);
        buffer.addElement(new Element());
        Assert.assertFalse(buffer.isEmpty());

    }

    @Test
    public void isFullTest() {
        int bufferCapacity = 1;
        Buffer buffer = new Buffer(bufferCapacity);
        Assert.assertFalse(buffer.isFull());
    }

    @Test
    public void isFullTestMax() throws BufferFullException{
        int bufferCapacity = 1;
        Buffer buffer = new Buffer(bufferCapacity);
        buffer.addElement(new Element());
        Assert.assertTrue(buffer.isFull());
    }

    @Test
    public void getCurrentLoadTest() {
        int bufferCapacity = 1;
        Buffer buffer = new Buffer(bufferCapacity);
        Assert.assertEquals(0 , buffer.getCurrentLoad());
    }

    @Test
    public void getCurrentLoadOneElementTest() throws BufferFullException{
        int bufferCapacity = 1;
        Buffer buffer = new Buffer(bufferCapacity);
        buffer.addElement(new Element());
        Assert.assertEquals(1 , buffer.getCurrentLoad());
    }

    @Test
    public void removeTest() throws BufferEmptyException , BufferFullException{
        int bufferCapacity = 2;
        Element element = new Element();
        Buffer buffer = new Buffer(bufferCapacity);
        buffer.addElement(element);
        Assert.assertEquals(element , buffer.removeElement());
        Assert.assertEquals(0 , buffer.getCurrentLoad());
    }

    @Test (expected = BufferEmptyException.class)
    public void removeMinTest() throws BufferEmptyException {
        int bufferCapacity = 1;
        Buffer buffer = new Buffer(bufferCapacity);
        buffer.removeElement();

    }

    @Test (expected = BufferFullException.class)
    public void addMaxTest() throws BufferFullException {
        int bufferCapacity = 0;
        Buffer buffer = new Buffer(bufferCapacity);
        buffer.addElement(new Element());
    }


}


