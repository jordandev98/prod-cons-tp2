package ca.qc.claurendeau.storage;

import ca.qc.claurendeau.exception.BufferEmptyException;
import ca.qc.claurendeau.exception.BufferFullException;

import java.util.ArrayList;
import java.util.List;

public class Buffer {

    private int capacity;
    private boolean isEmpty;
    private List<Element> elements;

    public Buffer(int capacity) {
        this.capacity = capacity;
        isEmpty = true;
        elements = new ArrayList<>();
    }

    // returns the content of the buffer in form of a string
    public String toString() {
        // VOTRE CODE
        String string = null;
        return string;
    }

    // returns the capacity of the buffer
    public int capacity() {
        return capacity;
    }

    // returns the number of elements currently in the buffer
    public int getCurrentLoad() {
        return elements.size();
    }

    // returns true if buffer is empty, false otherwise
    public boolean isEmpty() {
        return elements.size()==0 ? true : false;
    }

    // returns true if buffer is full, false otherwise
    public boolean isFull() {
        return getCurrentLoad() == capacity ? true : false;
    }

    // adds an element to the buffer
    // Throws an exception if the buffer is full
    public synchronized void addElement(Element element) throws BufferFullException
    {
        if (capacity == elements.size()){
            throw new BufferFullException();
        }
        elements.add(element);
    }

    // removes an element and returns it
    // Throws an exception if the buffer is empty
    public synchronized Element removeElement() throws BufferEmptyException
    {
        if (elements.size() == 0 ){
            throw new BufferEmptyException();
        }
        return elements.remove(elements.size()-1);
    }
}
